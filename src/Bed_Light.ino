/*
 * ----------------------------------------------------------------------------------
 *  ESP-12E Pinout:
 * ----------------------------------------------------------------------------------
 *                    ------------------------
 *                  --| Reset       D1 (TX0) |-- Serial TX/Prog RX
 *     LDR (1 Mohn) --| ADC         D3 (RX0) |-- Serial RX/Prog TX
 *              VCC --| CHPD        D4 (SCL) |--
 *                  --| D16         D5 (SDA) |--
 *         HC-SR501 --| D14 (SCK)         D0 |-- Bootloader (low - program, high - normal)
 *    Lower LED FET --| D12 (MISO)  D2 (TX1) |--
 *    Upper LED FET --| D13 (MOSI)  D15 (SS) |-- GND (for normal startup)
 *                  --| VCC              GND |--
 *                    ------------------------
 * ----------------------------------------------------------------------------------
 * Notes:
 *  - HC-SR501 modded for 3.3v usage - Voltage reg removed, Vin and Vout pads bridged, protecton diode removed.
 *  - 200nF ceramic cap across pins 12 and 13 of HC-SR501's BIS0001 IC to stop ESP's high frequency
 *    noise (2.4gHz) from influencing gain stage and causing false triggers.
 *  - 10k pull-down on PIR output to help against false positives too.
 */

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServerSPIFFS.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <Ticker.h>

#include "mqtt_config.h"

#define UPPER_LED_PIN         13
#define LOWER_LED_PIN         12
#define PIR_PIN               14

#define PWM_STEPS             255
#define FADE_ON_TIME          4
#define FADE_OFF_TIME         7
#define LOWER_PIR_ON_TIME     10000

#define LIGHT_SAMPLE_RATE     1500
#define LIGHT_SAMPLE_COUNT    5
#define LIGHT_TRIGGER_LEVEL   200

#define MQTT_STATE_TOPIC      "light/bed/state"
#define MQTT_COMMAND_TOPIC    "light/bed/command"

const int pwmMap[256] = {
  0, 1, 1, 1, 2, 2, 3, 3, 4, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 15, 15,
  16, 17, 17, 18, 19, 19, 20, 21, 22, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 41,
  42, 43, 44, 46, 47, 48, 50, 51, 52, 54, 55, 57, 58, 60, 61, 63, 65, 66, 68, 70, 71, 73, 75, 77, 79, 81, 83, 85,
  87, 89, 91, 93, 95, 97, 99, 101, 104, 106, 108, 110, 113, 115, 118, 120, 123, 125, 128, 130, 133, 136, 138, 141,
  144, 147, 150, 153, 155, 158, 161, 164, 168, 171, 174, 177, 180, 184, 187, 190, 194, 197, 201, 204, 208, 211,
  215, 219, 222, 226, 230, 234, 238, 242, 246, 250, 254, 258, 262, 266, 271, 275, 279, 284, 288, 292, 297, 302,
  306, 311, 316, 320, 325, 330, 335, 340, 345, 350, 355, 360, 365, 371, 376, 381, 387, 392, 398, 403, 409, 414,
  420, 426, 432, 438, 443, 449, 455, 462, 468, 474, 480, 486, 493, 499, 506, 512, 519, 525, 532, 539, 546, 552,
  559, 566, 573, 580, 588, 595, 602, 609, 617, 624, 632, 639, 647, 654, 662, 670, 678, 686, 694, 702, 710, 718,
  726, 735, 743, 751, 760, 768, 777, 786, 794, 803, 812, 821, 830, 839, 848, 857, 867, 876, 885, 895, 904, 914,
  924, 933, 943, 953, 963, 973, 983, 993, 1003, 1014, 1023 };

enum { IDLE, START, FADE, FADE_ON, FADE_OFF, DELAY };
uint8_t upperState = IDLE;
uint8_t lowerState = IDLE;

uint8_t upperCurrent = 0;
uint8_t upperTarget = 0;
uint16_t upperTimeInterval = 0;
long upperLastTime = 0;

uint8_t lowerCurrent = 0;
uint8_t lowerTarget = 0;
uint16_t lowerTimeInterval = 0;
long lowerLastTime = 0;
long lowerOnTime = 0;

MDNSResponder mdns;
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

WiFiClient mqttClient;
PubSubClient mqtt(mqttClient);

uint16_t lightSamples[LIGHT_SAMPLE_COUNT] = { 0 };
uint16_t lightAvg = 0;
uint16_t lightTotal = 0;
Ticker lightSample_tckr;

void setup()
{
  Serial.begin(115200);

  // Setup and initialize pins
  pinMode(UPPER_LED_PIN, OUTPUT);
  pinMode(LOWER_LED_PIN, OUTPUT);
  analogWrite(UPPER_LED_PIN, 0);
  analogWrite(LOWER_LED_PIN, 0);

  // Connect to WiFi
  WiFiManager wifiManager;
  if (!wifiManager.autoConnect("Bed Light")) ESP.reset();

  // Start servers
  mdns.begin("bedlight");
  httpUpdater.setup(&server);
  server.begin();

  // Connect to MQTT
  mqtt.setServer(MQTT_HOST, MQTT_PORT);
  mqtt.setCallback(mqttCallback);
  mqttReconnect();

  // Attach interrupt to PIR pin on rising edge
  attachInterrupt(PIR_PIN, PIR_trig, RISING);

  // Start taking light samples
  sampleLight();
  lightSample_tckr.attach_ms(LIGHT_SAMPLE_RATE, sampleLight);
}

void loop()
{
  // Handle servers
  if (!mqtt.connected()) mqttReconnect();
  if (mqtt.connected()) mqtt.loop();
  server.handleClient();

  // Handle LEDs
  switch(upperState)
  {
    case IDLE: break;

    case START:
      upperLastTime = millis();
      upperState = FADE;

      if (lowerCurrent > 0) 
      {
        lowerTarget = 0;
        lowerTimeInterval = FADE_OFF_TIME * 3;
        lowerLastTime = millis();
        lowerState = FADE_OFF;
      }
    break;

    case FADE:
      if (millis() - upperLastTime > upperTimeInterval)
      {
        if (upperCurrent < upperTarget) upperCurrent++;
        else if (upperCurrent > upperTarget) upperCurrent--;
        else { upperState = IDLE; break; }
        analogWrite(UPPER_LED_PIN, pwmMap[upperCurrent]);
        upperLastTime = millis();
      }
      break;
  }

  switch(lowerState)
  {
    case IDLE: break;

    case START:
      lowerLastTime = millis();
      if (lowerCurrent < lowerTarget) lowerState = FADE_ON;
      else if (lowerCurrent > lowerTarget) lowerState = FADE_OFF;
      else
      {
        lowerOnTime = millis();
        lowerState = DELAY;
      };
      lightSample_tckr.detach();
    break;

    case FADE_ON:
      if (millis() - lowerLastTime > lowerTimeInterval)
      {
        if (lowerCurrent < lowerTarget) lowerCurrent++;
        else
        {
          lowerOnTime = millis();
          lowerState = DELAY;
          break;
        }
        analogWrite(LOWER_LED_PIN, pwmMap[lowerCurrent]);
        lowerLastTime = millis();
      }
    break;

    case FADE_OFF:
      if (millis() - lowerLastTime > lowerTimeInterval)
      {
        if (lowerCurrent > lowerTarget) lowerCurrent--;
        else
        {
          lowerState = IDLE;
          lightSample_tckr.attach_ms(LIGHT_SAMPLE_RATE, sampleLight);
          break;
        }
        analogWrite(LOWER_LED_PIN, pwmMap[lowerCurrent]);
        lowerLastTime = millis();
      }
    break;

    case DELAY:
      if (millis() - lowerOnTime > LOWER_PIR_ON_TIME)
      {
        lowerTarget = 0;
        lowerTimeInterval = FADE_OFF_TIME * 3;
        lowerLastTime = millis();
        lowerState = FADE_OFF;
      }
    break;
  }
}

void PIR_trig()
{
  Serial.println("PIR Trig!");
  if (lightAvg < LIGHT_TRIGGER_LEVEL && upperCurrent == 0)
  {
    lowerTarget = 16;
    lowerTimeInterval = FADE_ON_TIME;
    lowerState = START;
  }
}

void mqttReconnect()
{
  static long lastReconnect = 0;

  if (millis() - lastReconnect > 5000)
  {
    if (mqtt.connect("Bed Light", MQTT_USER, MQTT_PASS))
    {
      mqtt.subscribe(MQTT_COMMAND_TOPIC);

      DynamicJsonBuffer json;
      JsonObject &root = json.createObject();
      root["state"] = (upperTarget > 0 ? "ON" : "OFF");
      root["brightness"] = upperTarget;

      char buffer[root.measureLength() + 1];
      root.printTo(buffer, sizeof(buffer));
      mqtt.publish(MQTT_STATE_TOPIC, buffer, true);

      lastReconnect = 0;
    }
    else lastReconnect = millis();
  }
}

void mqttCallback(char* topic, byte* payload, unsigned int length)
{
  char payload_assembled[length];
  for (int i = 0; i < length; i++) payload_assembled[i] = (char)payload[i];

  DynamicJsonBuffer json;
  JsonObject& root = json.parseObject(payload_assembled);

  if (!root.success())
  {
    Serial.println("Unable to parse JSON");
    return;
  }

  if (root.containsKey("state"))
  {
    if (!strcmp(root["state"], "ON"))
    {
      upperTarget = PWM_STEPS;
      upperTimeInterval = FADE_ON_TIME;
      upperState = START;
    }
    else if (!strcmp(root["state"], "OFF"))
    {
      upperTarget = 0;
      upperTimeInterval = FADE_OFF_TIME;
      upperState = START;
    }
    else if (!strcmp(root["state"], "wakeup"))
    {
      upperTarget = PWM_STEPS;
      upperTimeInterval = 3500;
      upperState = START;
      root["state"] = "ON";
      root["brightness"] = 255;
    }
  }

  if (root.containsKey("brightness")) upperTarget = constrain(root["brightness"], 0, 255);

  if (!root.containsKey("brightness")) root["brightness"] = upperTarget;

  char buffer[root.measureLength() + 1];
  root.printTo(buffer, sizeof(buffer));
  mqtt.publish(MQTT_STATE_TOPIC, buffer, true);
}

void sampleLight()
{
  static uint8_t index = 0;

  lightTotal -= lightSamples[index];
  lightSamples[index] = analogRead(A0);
  lightTotal += lightSamples[index];
  lightAvg = lightTotal / LIGHT_SAMPLE_COUNT;

  Serial.print(lightSamples[index]);
  Serial.print(", Avg: ");
  Serial.println(lightAvg);

  if (++index >= LIGHT_SAMPLE_COUNT) index = 0;
}
